# ToDo List

### Descrição do Projeto

**Responsável**: Emerson

Um aplicativo onde possa ser feita listas, tanto de compras, quanto de coisas
para serem feitas no dia-a-dia (To-do list) , que tenha como separar a lista de
comprar por categorias, mostre o nome de produtos possíveis para compra, que
tenha como remover o item e que no momento em que em tenha feito a compra possa
confirmar que ele foi pego. Já para a lista de coisas para fazer, que elas
sejam separadas em tópicos, contenha a data e que possa confirmar caso ela já
tenha sido realizado. Manter os dados das listas guardados para conferência e
para que possa reutiliza-lá.

### Modelagem

**Responsável**: Kelvin

Usuário insere atividade na lista (posição 1 é ocupada, variavel de
posição incrementada), se é uma compra marca um checkbox.

Se a atividade é uma compra é agrupada acima, junto com outras compras.

Usuário pode remover a atividade da lista no botão X

As atividades guardam a hora quando você marca que foi concluida.

Usuário altera suas atividades na lista marcando que já foi feita.

Os itens ficam em uma tabela para consulta novamente quando precisar.

v2:
*ATIVIDADE

nº

buy/sell (checkbox)

delete 

horario inicial

horario max

horario conclusão

está concluido (y/n)

editar

*arquivo

tabela

### Prototipagem

**Responsável**: Ismael Woltmann